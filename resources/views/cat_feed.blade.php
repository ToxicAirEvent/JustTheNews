<?php
//Loads helperFunctions controller so the sourceURLStripper function can be called to show source URLs.
use App\Http\Controllers\helperFunctions;
?>

@extends('layouts.app')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = $cat->category_name . " News and Updates";
	$page_meta_description = $cat->category_meta_description;
?>

@section('content')
<div class="row">
	<div class="col-xs-8 col-md-10">
		<h1 class="page_title">{{$cat->category_name}}</h1>
		@if($cat->category_description != null)
			<p>{{$cat->category_description}}</p>
		@else
			<h5 class="page_subtitle">&nbsp;- {{env("ALL_POST", 25)}} of the most recent stories relating to {{$cat->category_name}}:</h5>
		@endif
		
		@if(Auth::user() && Auth::User()->role == 'admin')
			<p><a href="/edit-category/{{$cat->category_id}}">Edit: {{$cat->category_name}}</a></p>
		@endif
	</div>
	
	<div class="col-xs-4 col-md-2">
		<p><small class="source"><i>Times in US East</i></small></p>
	</div>
</div>
  
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
		@foreach($news_items['items'] as $item)
			@if(helperFunctions::hourAhead($item->get_date('Y-m-d G:i:s')) === false)
			<div class="item">
				<h3 class="story_title">
					<a href="{{ $item->get_permalink() }}" class="story_link" target="_blank" onClick="ga('send','event','News Story Viewed','Category Feed','{{$cat->category_name}}');">
						{!! $item->get_title() !!}
					</a> 
					- 
					<span class="source">(<a href="//{{helperFunctions::sourceURLStripper($item->get_permalink())}}" title="Go to {{helperFunctions::sourceURLStripper($item->get_permalink())}}" target="_blank">{{helperFunctions::sourceURLStripper($item->get_permalink())}}</a>)</span>
				</h3>
				<div class="story_content">
					{!! $item->get_content() !!}
					<span class="read_more">LONG READ - CLICK FOR MORE &#x25BC;</span>
				</div>
				<p class="ptl pbs mbn"><small><span class="story_time_posted" data-time="{{$item->get_date('Y-m-d H:i:s')}}">{{$item->get_date('F j, Y - g:i a T')}}</span></small> | <span class="modpoint_manager"></span><span class="modpoint_message"></span></p>
			</div>

			<div class="clear"></div>
			@endif
		  @endforeach
	</div>
	
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
		<h3>News From:</h3>
		<ul>
			@foreach($sources as $source)
				<li><a href="{{$source->source_home_url}}" target="_blank">{{$source->source_name}}</a></li>
			@endforeach
		</ul>
		
		@if(!Auth::check())
			@include('interstitials.sidebar')
		@endif  
	</div>
</div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/js/modpoints_listings_page.js"></script>
<script src="/js/showExistingVotes.js"></script>
<script src="/js/contentExpander.js"></script>
@endsection