@extends('layouts.app')

@section('content')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Create a new category.";
	$page_meta_description = "Create a new category.";
?>

	<div class="col-xs-12 col-md-10">
		<div class="panel-body">
			<!-- Display Validation Errors -->
			@include('common.errors')

			<!-- Edit Source Form -->
			<form action="/create_new_category" method="POST" class="form-horizontal">
				{{ csrf_field() }}

				<!-- Edit Source Info Fields -->
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Name</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_name" id="source_name" class="formWidth">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category URL</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_url" id="source_home_url" class="formWidth">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Slug</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_slug" id="category_slug" class="formWidth">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Description</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<textarea name="category_description" id="category_description" rows="3" cols="33"></textarea>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Meta Description</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<textarea name="category_meta_description" id="category_meta_description" rows="2" cols="33"></textarea>
					</div>
				</div>

				<!-- Update Source Button -->
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" class="btn btn-default">
							<i class="fa fa-plus"></i> Create New Category
						</button>
					</div>
				</div>
			</form>
			<p>Note: New categories cannot be set to be on the front page in order to prevent them from showing up blank.</p>
		</div>
	</div>
@endsection