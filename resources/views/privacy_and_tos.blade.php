@extends('layouts.app')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Privacy, Terms of Service, and Other Information";
	$page_meta_description = "Privacy and terms of service information for " . env('APP_NAME', 'Orderly.News') . ". As well as an FAQ and list of currently serviced news feeds from the worlds top sources.";
?>

@section('content')

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 id="privacy">Privacy Policy</h1>
		<p>{{env('APP_NAME', 'Orderly.News')}} is provided as is and is subject to future updates and changes. In order to provide various services for our website we require a login through <a href="https://developers.facebook.com/docs/facebook-login">Facebook</a> or <a href="https://developers.google.com/identity/sign-in/web/sign-in">Google's oAuth API's.</a></p>
		<p>{{env('APP_NAME', 'Orderly.News')}} stores only data requested on oAuth consent screens upon login. This data primarily first name, last name, and email address. This is subject to change in the future. You will be notified and asked to consent to further data collection.</p>
		<p>{{env('APP_NAME', 'Orderly.News')}} reserves the right for our staff members to use this data for any purposes internally. We will not disclose or publically display any of this data.</p>
		<p>By logging into {{env('APP_NAME', 'Orderly.News')}} you are consenting to this privacy policy.</p>

		<hr>

		<h1 id="terms">Terms Of Service</h1>
		<p>{{env('APP_NAME', 'Orderly.News')}} is provided as is with no warranty.</p>
		<p>Our service is provided free of charge and as a result we reserve the right to terminate your service at any time for any reason.</p>
		<p>Attempts to circumvent any termination of service will result in termination of any account used to get around a prior termination of service.</p>
		<p>In order to use the services provided by this platform you must consent to the {{env('APP_NAME', 'Orderly.News')}} privacy policy.</p>

		<hr>

		<p>Please note that {{env('APP_NAME', 'Orderly.News')}} is in its infancy and as a result features and functionality of the site are subject to change without notice at any time.</p>

		<hr>

		<h1 id="sources">Current List Of News Sources</h1>
		<p>We currently run {{env('APP_NAME', 'Orderly.News')}} on an approved services basis. Keeping the number of feeds we crawl limited helps us find and workout various kinks and bugs in the system.</p>
		<p>Below you'll find a list of all the feeds we currently crawl and support here on <em>{{env('APP_NAME', 'Orderly.News')}}.</em> You can see the latest news from all services by visiting our <a href="/news/all"><i>/all</i> feed.</a>
		You can also choose which services you see news from by <a href="/subscriptions">managing your subscriptions</a> and then viewing them at <a href="/news"><i>/news.</i></a>

		<ul>
			{{-- List off approved news sources into the page. --}}
			@if(count($news_sources) > 0)
				@foreach($news_sources as $news_source)
					<li>{{$news_source->source_name}}</li>
				@endforeach
			@endif
		</ul>

		<p>Have a feed you'd like to see that we currently don't support?</p>
		<p>Send us an email with a link to your suggestion at <a href="mailto:suggestions@orderly.news">suggestions[at]orderly.news</a></p>
	</div>
</div>

@endsection