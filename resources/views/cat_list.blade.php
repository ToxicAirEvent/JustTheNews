@extends('layouts.app')

@section('content')
<?php
	//PHP snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "All Categories";
	$page_meta_description = "News spans many different genres, interest, and viewpoints. We've got you covered for whatever you're interested in with categories ranging from arts and entertainment to US politics, to science and technology.";
?>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="page_title">Categories</h1><h3 class="page_subtitle">&nbsp;- A Complete List of All News Categories:</h3>
		<p></p>
	</div>
</div>

<div class="row">
	@foreach($all_categories as $category)
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h2><a href="/cat/{{$category->category_url}}" title="View the news feed of the latest stories in {{$category->category_name}}" onClick="ga('send','event','Category Listings','Category Title','{{$category->category_name}}');">{{$category->category_name}}</a></h2>
			@if($category->category_description != null)
				<p>{{$category->category_description}}</p>
			@else
				<p>View <a href="/cat/{{$category->category_url}}" onClick="ga('send','event','Category Listings','Descriptionless Category','{{$category->category_name}}');">All Stories about {{$category->category_name}}</a>.</p>
			@endif
			
			@if(Auth::user() && Auth::User()->role == 'admin')
				<p><a href="/edit-category/{{$category->category_id}}">Edit {{$category->category_name}}</a></p>
			@endif
		</div>
	@endforeach
</div>
@endsection