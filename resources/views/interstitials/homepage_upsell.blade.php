<div class="row">
	<div class="clear pvl"></div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 deep_blue_block">
		<h2 class="site_title white h1"><span class="glyphicon glyphicon-globe"></span>&nbsp;Want The News Your Way? Signin To Customize Your Own Personal News Feed!</h2>
	</div>
</div>
	
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<p class="ptm"><a href="/register" onClick="ga('send','event','Intersitials','Account Action','Registration Page">Sign up for {{env('APP_NAME', 'Orderly.News')}}</a> to personalize your own news feed to feature top stories from favorite news sources on topics that interest you.</p>
		<p>Get the latest stories from top reporters at {{$random_sources[0]}}, {{$random_sources[1]}}, and {{$random_sources[2]}}, or browse recent updates on a wide range of topics with stories covering everything from  
		{{$random_categories[0]}} to {{$random_categories[1]}}, or {{$random_categories[2]}}.</p>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
		<a href="{{ url('/auth/google') }}" class="btn btn-google" onClick="ga('send','event','Intersitials','Account Action','Register - Google">
			<img style="max-width: 250px;" src="/img/google-sign-in.png" />
		</a>
	</div>
	
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		<a href="{{ url('/auth/facebook') }}" class="btn btn-facebook" onClick="ga('send','event','Intersitials','Account Action','Register - Facebook">
			<img style="max-width: 250px;" src="/img/login_with_facebook.png" />
		</a>
	</div>
</div>
	
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<p>Want to see a full list of news service providers you can subscribe to before you sign up? We've got you covered.
		<a href="/about" onClick="ga('send','event','Intersitials','Link Action','Homepage Read FAQ">Read our FAQ, and check out our ever expanding list of covered news sources.</a></p>
	</div>
</div>
	
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<hr>
	</div>
</div>