<h3 class="site_title white h2 deep_blue_block pvs phm"><span class="glyphicon glyphicon-globe"></span>&nbsp;Want The News Your Way? Signin To Customize Your Own Personal News Feed!</h3>
	
<p>The <b>All News Stories</b> feed is a great way to get a birds eye view of what's going on in the world right now.</p>
<p>However sometimes you might want to just read stories on topics, and from sources that interest you the most.</p>
<p>Signing up for an account on {{env('APP_NAME', 'Orderly.News')}} will allow you to create a custom news feed concisting only of stories from your
favorite news sources. Signup now. It takes only a few clicks.</p>

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
	<a href="{{ url('/auth/google') }}" onClick="ga('send','event','Intersitials','Account Action','Feed Ad Register - Google">
		<img class="img-responsive" src="/img/google-sign-in.png" />
	</a>
</div>

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	<a href="{{ url('/auth/facebook') }}" onClick="ga('send','event','Intersitials','Account Action','Feed Ad Register - Facebook">
		<img class="img-responsive mtm" src="/img/login_with_facebook.png" />
	</a>
</div>

<div class="clear pvm"></div>

<p>Want to see a full list of news service providers you can subscribe to before you sign up? We've got you covered.
<a href="/about" onClick="ga('send','event','Intersitials','Link Action','Feed Ad Read FAQ">Read our FAQ, and check out our ever expanding list of covered news sources.</a></p>