@extends('layouts.app')

@section('content')

<?php
	//Php snippet is reported back to the app.blade template. Settings these variables sets the page title and meta description.
	$page_title = "Edit Category: " . $category_core_information->category_name;
	$page_meta_description = "Editing information for " . $category_core_information->category_name;
?>

<div class="row">
	<div class="col-xs-12 col-md-10">
		<div class="panel-body">
			<!-- Display Validation Errors -->
			@include('common.errors')
			
			<!-- Edit Source Form -->
			<form action="/updated_existing_category" method="POST" class="form-horizontal">
				{{ csrf_field() }}

				<!-- Edit Source Info Fields -->
				<input type="hidden" name="category_id_number" id="category_id_number" value="{{$category_core_information->category_id}}">
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Name</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_name" id="category_name" class="formWidth" value="{{$category_core_information->category_name}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category URL</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_url" id="category_url" class="formWidth" value="{{$category_core_information->category_url}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Slug</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="text" name="category_slug" id="category_slug" class="formWidth" value="{{$category_core_information->category_slug}}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Description</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<textarea name="category_description" id="category_description" rows="3" cols="33">@if($category_core_information->category_description != null){{$category_core_information->category_description}}@endif</textarea>
					</div>
				</div>
			
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Category Meta Description</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<textarea name="category_meta_description" id="category_meta_description" rows="2" cols="33">@if($category_core_information->category_meta_description != null){{$category_core_information->category_meta_description}}@endif</textarea>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Frontpage:</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="radio" name="show_front_page" value="1" @if($category_core_information->front_page == 1)checked @endif> Yes
						<input type="radio" name="show_front_page" value="0" @if($category_core_information->front_page != 1)checked @endif> No
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-md-3">
						<p class="control-label text-right"><strong>Frontpage Featurette:</strong></p>
					</div>
					
					<div class="col-xs-12 col-md-9">
						<input type="radio" name="front_page_featurette" value="1" @if($category_core_information->front_page_feature == 1)checked @endif> Yes
						<input type="radio" name="front_page_featurette" value="0" @if($category_core_information->front_page_feature != 1)checked @endif> No
					</div>
				</div>

				<!-- Update Source Button -->
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" class="btn btn-default">
							<i class="fa fa-plus"></i> Update Category
						</button>
					</div>
				</div>
			</form>
			<!-- End Edit Source Form -->
		</div>
	</div>
</div>
@endsection