<?php

use App\source;
use App\subscription;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//A basic route to the homepage. For now this is very sparse in content but eventually we'll get a full functioning homepage.
Route::get('/','showFeeds@homePageFeeds');

//Lays down the Authentication routes. This is just a helper class for routes that require user authentication.
Auth::routes();

//Routes logged-in users to the user home page. Again not much is here at the moment.
Route::get('/news', 'showFeeds@usersChoosenFeeds')->middleware('auth');

//These routes exist to handle callback information for the Socialite oAuth 2.0 Classes that allow users to login with Google and Facebook.
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

//When users visit the /logout page the auth class will log them out. They'll then be redirected to the site homepage.
Route::get('logout',function(){
	//Handle the logout
	Auth::logout();
	return redirect('/');
});

//Shows latest news from approved sources in order.
Route::get('news/all','showFeeds@allFeeds');

//Get all news from a particular category.
Route::get('/cat/{category_url}','showFeeds@categoryFeed');
Route::get('/categories','categoryListings@CompleteCategoryList');


//Passes list of approved news sources to Privacy and TOS page.
Route::get('privacy-and-tos',function(){

	//Calls on source model to get sources from the database.
	$news_services_source_list = \App\source::orderBy('source_name','ASC')->get();

	//returns the privacy_and_tos blade and passes the news sources list to the page.
	return view('privacy_and_tos',['news_sources' => $news_services_source_list]);
});

Route::get('about', function(){
	//Calls on source model to get sources from the database.
	$news_services_source_list = \App\source::orderBy('source_name','ASC')->get();
	
	return view('faq',['news_sources' => $news_services_source_list]);
});

//This middleware group exist to create routes only accessible to admins.
Route::group(['middleware' => 'App\Http\Middleware\AdminActions'], function()
{

	//Lets admins view the form to create new sources
    Route::get('/new-source', function(){
        return view('newSource');
    });
	
	//Handles the insertion of a new source.
	Route::post('new-source-added','\App\source@saveNewSource');
	
	//Lets admins view the form to edit existing sources
    Route::get('/edit-source/{source_id}','sourcesController@editSourceInfo');
	
	//Updates and existing source.
	Route::post('updated_existing_source','\App\source@updateSource');
	
	//Create new category form and validation route
	Route::get('new-category', function(){
		return view('newCategory');
	});
	Route::post('/create_new_category','categoryManager@createNewCategory');

	//Lets admins view the form to edit existing categories
    Route::get('/edit-category/{category_id}','categoryManager@editCategoryInfo');
	
	//Updates and existing category.
	Route::post('updated_existing_category','categoryManager@updateCategoryData');
	
	Route::get('add-category-link/{source_id}/{category_id}','categoryManager@linkSource');
	Route::get('remove-category-link/{source_id}/{category_id}','categoryManager@removeCategoryLink');
});


/* Temp routes to familiarize myself with how to pass route parameters to functions */
Route::get('subscriptions','subscriptions@userSubscriptionsList')->middleware('auth');


Route::get('add-subscription/{feed_id}','\App\subscription@subscribeToFeed');
Route::get('remove-subscription/{unsub_feed_id}','\App\subscription@unsubscribeFromFeed');

/* Routes that will handle modpoints. */
Route::post('add_post_modpoint','modpoints\modpointsController@modPointManager');
Route::get('popular-articles-and-stories','modpoints\modpointStoryListing@recentlyModded');
Route::get('api/stories','modpoints\modpointStoryListing@recentPostAPI');