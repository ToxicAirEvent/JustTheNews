<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//Only uncomment this for when working on NFO servers.
//use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Only uncomment this for when working on NFO servers.
		//Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
