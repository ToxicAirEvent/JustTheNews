<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class story extends Model
{
    //Normally the plural of the model is the table the model assumes in MySQL. However 'storys' isn't a work. So we'll point story to the stories table using a protected $table variable.
	protected $table = 'stories';
}
