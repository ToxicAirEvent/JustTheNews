<?php

namespace App;

use App\source;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class subscription extends Model
{

	//Function will allow a user to subscribe to a feed.
	public function subscribeToFeed($feed_id){
	
		$signed_in_user_id = \Auth::id();
	
		//Checks that the user is signed in and the feed they are looking for exist.
		if(Auth::check() && source::where('source_id', '=', $feed_id)->exists()){
		
			$sub_status_info = $this->subscriptionExistanceChecker($feed_id,$signed_in_user_id);
		
			//Makes sure the user is not subscribed to that feed already. If they aren't add the subscription.
			if ($sub_status_info["sub_status"] == false) {
				$this->insert(['user_id' => $signed_in_user_id, 'news_source_id' => $feed_id]);
				echo "You should now be subscribed to that feed.";
			}
		}else{
			echo "Either you are not signed in or that feed does not exist. Try again.";
			echo "-----";
		}
	}
	
	//Function unsubscribes a user from a feed.
	public function unsubscribeFromFeed($unsub_feed_id){
	
		$check_signin_status = \Auth::id();
	
		//Checks that the user is signed in and the feed they are looking for exist.
		if(Auth::check() && source::where('source_id', '=', $unsub_feed_id)->exists()){
		
			$sub_status_info = $this->subscriptionExistanceChecker($unsub_feed_id,$check_signin_status);
		
			//Make sure the user currently signed in is subscribed to that feed.
			if ($sub_status_info["sub_status"] == true) {
				echo "You are subscribed to that feed already. You will be unsubscribed.<br>";
				$this->where('subscription_id','=',$sub_status_info["unique_sub_id"])->delete();
				echo "You are now unsubscribed.";
			}
		}else{
			echo "Either you could not be unsubscribed either because that feed does not exist, or you are not logged in.";
			echo "-----";
		}
	}
	
	/* 
		This function exist within this class to be used to validate if the subscription that is trying to be created, or modified exist or not.
		It will return TRUE if the subscription exist and FALSE if it does not.
		If the subscription does exist the unique_sub_id will be returned with the subscription ID. Otherwise it will be null.
	*/
	private function subscriptionExistanceChecker($sub_id, $user_id){
		
		$user_subs = $this->where('user_id', '=', $user_id)->get();

		if($user_subs->contains('news_source_id', $sub_id)){
		
		$unique_sub_id = $user_subs->where('news_source_id','=',$sub_id)->pluck('subscription_id')->toArray()[0];
		
			echo "You are subscribed to that feed.<br>\n";
			$sub_final_info = ["sub_status" => true, "unique_sub_id" => $unique_sub_id];
			
		}else{
			echo "You are not subscribed to that feed.<br>\n";
			$sub_final_info = ["sub_status" => false, "unique_sub_id" => null];
		}
		
		//Return array with sub status. If the subscription exist return the unique subscription ID as well.
		return $sub_final_info;
	}
}