<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\source;
use App\category;

class sourcesController extends Controller
{	

	public function editSourceInfo($source_id){
	
		$source_fetch = new source();
		$source_core_information = $source_fetch->fetchExistingSource($source_id);
	
		$category_data = new category();
		
		$category_listings = $category_data->getCategoriesList();
		$category_connections = $category_data->getExistingConnections($source_id);
	
		return view('editSource',['source_core_information' => $source_core_information,'category_listings' => $category_listings,'category_connections' => $category_connections]);
	}
}
