<?php

namespace App\Http\Controllers\modpoints;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

//Import the story model for this controller.
use App\story;

//Just import everything date and time related from php becuse laravel gets upset if you don't.
use DateTime;
use DatePeriod;
use DateIntercal;

class modpointStoryListing extends Controller
{
	//Create a variable to store modpoints in.
	protected $mod_points;
	
	//Once modpoints is created we'll want to first assign it values so we have something to reference when we want to count modpoints.
	public function __construct(){
		$this->mod_points = \DB::table('mod_points')->get();
	}
	
    public function recentlyModded(){
		
		$story_data = story::orderBy('date','desc')->simplePaginate(env('POST_PER_PAGE', 10));
		
		
		//dd($mod_points);
		
		$story_data->map(function($story_votes){
			
			$interesting_votes = $this->votes($story_votes->id,1);
			$funny_votes = $this->votes($story_votes->id,2);
			$relevant_votes = $this->votes($story_votes->id,3);
			
			$story_votes->interesting_votes = $interesting_votes;
			$story_votes->funny_votes = $funny_votes;
			$story_votes->relevant_votes = $relevant_votes;
			
			return $story_votes;
		});
		
		return view('story',['feed_data' => $story_data]);
	}
	
	private function votes($post_id,$mod_point_type){
		$mod_point_data_set = $this->mod_points;
		
		$find_by_point_type = $mod_point_data_set->where('story_id','=',$post_id)->where('mod_type','=',$mod_point_type);
		$counted_points = count($find_by_point_type);
		
		return $counted_points;
	}
	
	public function recentPostAPI(){
		
		$expire_stories = now()->addMinutes(7);
		
		$story_cache = Cache::remember('story_and_modpoint_cache',$expire_stories,function(){
			$story_data = story::orderBy('date','desc')->take(env('USER_POST', 100))->get();
			
			$story_data->map(function($story_votes){
				
				$interesting_votes = $this->votes($story_votes->id,1);
				$funny_votes = $this->votes($story_votes->id,2);
				$relevant_votes = $this->votes($story_votes->id,3);
				
				$story_votes->interesting_votes = $interesting_votes;
				$story_votes->funny_votes = $funny_votes;
				$story_votes->relevant_votes = $relevant_votes;
				
				return $story_votes;
			});
			
			$story_data->toArray();
			return $story_data;
		});
		
		echo json_encode($story_cache);
	}
}
