<?php

namespace App\Http\Controllers\modpoints;

//Laravel base classes.
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

//Import the story model for this app.
use App\story;

use App\Http\Controllers\helperFunctions;

//Imports the Carbon time management library.
use Carbon\Carbon;

class modpointsController extends Controller
{	

	private $debug_mode = 0;

	//Handles javascript post request for addmin mod points and then validates them.
    public function modPointManager(Request $story_data){
		
		//Validate that all of the request criteria for making a new point are met.
		$point_validator = Validator::make($story_data->all(), [
			'story_title' => 'required|string',
			'story_url' => 'required|url',
			'point_type' => 'required|integer',
			'story_date_time' => 'required|date_format:"Y-m-d H:i:s"'
		]);
		
		/* LETS START OVER IN THIS LOGIC! */
		//Is the user logged in and did the validator checkout?		
		if ($point_validator->fails() || Auth::check() != true){
			$basic_verify = 0;
		}else{
			$basic_verify = 1;
		}
		
		//Is the logged in user even allowed to vote right now?
		If($basic_verify && $this->userEligibleVote()){
			$redshirt = 1;
		}else{
			$redshirt = 0;
			
			//If the basic verification fails then we just want to tell them to login. If all checks out they are just time restricted tell them how long until they can vote.
			if($basic_verify === 0){
				$returned_status = ["voted" => false,"reason" => "Please <a href=\"/login\">login</a> or <a href=\"/register\">register</a> to vote.","remove_buttons" => 0];
			}else{
				$time_to_vote = $this->timeToVote();
				$vote_message = "You are currently at the voting threshold. You can vote again in " . $time_to_vote . " minutes.";
				if($this->debug_mode === 1){echo "You can't vote on this story due to time constraints.";}
				$returned_status = ["voted" => false,"reason" => $vote_message,"remove_buttons" => 0];
			}
		}
		
		//If the most basic checks are cleared and the user is allowed to vote make their vote placement eligability equal to 1.
		if($redshirt === 1 && $basic_verify === 1){
			$vote_placement = 1;
		}else{
			$vote_placement = 0;
		}
		
		if($vote_placement === 1){
			$request_story = story::where('url','=',$story_data->story_url)->first();
			
			//If the story doesn't exist let's create it and assign a mod point for it to the user.
			if($request_story === null){
				$this->createStoryThenMod($story_data);
				$returned_status = ["voted" => true,"reason" => "You Are The First To Vote On This Story!","remove_buttons" => 1];
				if($this->debug_mode === 1){echo "Your modpoint has been placed. You are the first to vote on this story.";}
				
				
			//If the story already exist lets see if the user already voted on it. If they haven't lets allow them to do so.
			}else if($this->alreadyVotedOnStory($request_story->id,Auth::user()->id) === false){
				$this->addModPoint($request_story->id,$story_data->point_type);
				$returned_status = ["voted" => true,"reason" => "Your Mod Point Has Been Placed!","remove_buttons" => 1];
				if($this->debug_mode === 1){echo "You mod point has been placed on this story.";}
			
			//If the user already voted lets tell them they've done so.
			}else{
				if($this->debug_mode === 1){echo "You can't vote on this story due to time constraints.";}
				$returned_status = ["voted" => false,"reason" => "You already voted on this story.","remove_buttons" => 1];
			}
		}

		echo json_encode($returned_status);
	}
	
	//If a story doesn't exist it needs to be created and then have the modpoint assigned.
	private function createStoryThenMod($new_story){
		
		//Just for the sake of arguement let's remove any tags that could exist in the story title before we put it into the database.
		$story_title = strip_tags($new_story->story_title);
		$story_title = filter_var($story_title, FILTER_SANITIZE_STRING);
		$story_title = trim($story_title);
		
		$source_url = helperFunctions::sourceURLStripper($new_story->story_url);
		
		story::insert(['title' => $story_title,'url' => $new_story->story_url,'date' => $new_story->story_date_time,'source_url' => $source_url,'created_at' => date("Y-m-d H:i:s"),'user_id' => Auth::user()->id]);
		$newly_created_story = story::where('url','=',$new_story->story_url)->first();
		$this->addModPoint($newly_created_story->id,$new_story->point_type);
	}
	
	//Adds a modpoint for a user and the current post into the mod_points pivot table.
	private function addModPoint($story_id,$modpoint_type){
		\DB::table('mod_points')->insert(['story_id' => $story_id,'user_id'=>Auth::user()->id,'mod_type' => $modpoint_type]);
	}
	
	//Logic to check if the user is even eligible to be voting at this very moment.
	private function userEligibleVote(){
		//User carbon to get the current time.
		$current_time = Carbon::now();
				
		$voted_time = Carbon::parse(Auth::user()->mod_point_placed);
		
		//If the user has their mod points set to null as is default or might be set in other situations let's start them off at 1.
		if(Auth::user()->mod_points_used == null){
			$current_mod_points = 1;
		}else{
			$current_mod_points = Auth::user()->mod_points_used;
			$current_mod_points = $current_mod_points + 1;
		}
		
		/*
			Handles some logic for if a user should be allowed to vote.
			Basically if the number of points placed is equal to null or more than an hour has passed since the last vote set votes to zero and place it.
			If an hour has not passed but the user is below the threshold place the vote.
			If none of these conditions are met return false and don't place the vote.
		*/
		if(Auth::user()->mod_point_placed === null || $current_time->gte($voted_time)){
			$this->updateUserVotingStats($current_time,0);
			return true;
		}else if(Auth::user()->mod_points_used < env('HOURLY_VOTE_LIMIT', 6)){
			$this->updateUserVotingStats($current_time,$current_mod_points);
			return true;
		}else{
			//If the user is at the point threshold and not enough time has passed prohibit them from voting.
			return false;
		}
	}
	
	//This handles updating the users current mod point stats. It's easiest to do this as a function so it can just be used over and over again instead of cluttering the existing logic.
	private function updateUserVotingStats($now,$current_mod_points){
		\DB::table('users')->where('id','=',Auth::user()->id)->update(['mod_point_placed' => $now->addHour(),'mod_points_used' => $current_mod_points]);
	}
	
	//Check to see if the current user has voted on this story.
	private function alreadyVotedOnStory($story_id,$user_id){
		
		//Get every instance of the story in the modpoints table and then see if the user has voted on it.
		$post_user_voting_history = \DB::table('mod_points')->where('story_id','=',$story_id)->where('user_id','=',$user_id)->get();
		$post_user_voting_history->toArray();
		
		if($this->debug_mode === 1){echo "[USER VOTING HISTORY COUNT: " . count($post_user_voting_history) . "]";}
		
		//Simply count the array. If nothing is returned it will be zero if they have it'll be greater than zero.
		//Return true if they've already voted false otherwise.
		if(count($post_user_voting_history) > 0){
			return true;
		}else{
			return false;
		}
	}
	
	//A simple carbon function which will tell how long until the user can place another vote on a story.
	private function timeToVote(){
		$vote_time = Auth::user()->mod_point_placed;
		$diff_time = Carbon::now()->diffInMinutes($vote_time);
		
		return $diff_time;
	}
}
