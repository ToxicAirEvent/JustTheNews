# Just The News
### An Categorized RSS Reader
Just The News is designed to list news articles from a variety of legitimate sources in the order the stories are posted.

I created this because I'm someone who always enjoyed twitter for it's ability to get stories from reputable sources as their timelines were updated. Twitter and a number of other sites have moved away from this canonical feed system to feed systems in which the results are taylored to you.

I'm not opposed to these systems as a whole. They've often times suggested interesting content from interesting people I might not have otherwise found.

I still like the ability to access information from many different view points and topics all at once without any background system attempting to present a view point it thinks I will most like. The ability to see these results in the order they are posted also can assist with the ability to built time lines of when events happened.

You can see Just The News up and running over at [orderly.news](https://orderly.news/ "orderly.news")

# Background
Just The News is built using the [Laravel Framework](https://laravel.com/ "Laravel Framework") and a number of other dependencies.
- [Socialite](https://github.com/laravel/socialite "Socialite")
- [Feeds](https://github.com/willvincent/feeds "Feeds") - A laravel dependency of [SimplePie](http://simplepie.org/ "SimplePie")
- [Hideseek](http://vdw.github.io/HideSeek/ "Hideseek")

# Usage
The application is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International license.](https://creativecommons.org/licenses/by-nc-nd/4.0/ "Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International license.")

People should be able to use this application to satisfy their needs. RSS feeds are used for tons of things all over the internet and the ability to crawl them in an ordered, grouped fashion may prove beneficial to some peoples work, or personal knowledge gathering.

It is asked though that you please don't modify this application and its underlying functionality as that could threaten the integrety of what the application stands to do.

If you have different goals or uses for RSS feed crawling I highly encourage you [use SimplePie.](http://simplepie.org/ "use SimplePie.")

For commercial use for functionality please feel free to get in touch with me at jackemceachern[at]gmail.com

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

# Setup
1. Have [Composer.](https://getcomposer.org/ "Composer.") installed and functioning.
1. Make sure your server has all of the nessecarry components for [Laravel.](https://laravel.com/docs/5.6/installation "Laravel.") as wekk as a MySQL database.
1. Download this repository and upload it to your server. Then run composer on this repository.
1. Once the application is installed you'll need to do some configuration to your apps .env file. There are a few custom parameters for this application.
	- `USER_POST=` - This sets how many post should be shown to a logged in user on their personal feed.
	- `ALL_POST=` - This sets how many post will be shown in the all post or post category feeds.
	- `FACEBOOK_ID=` & `GOOGLE_ID=` - [Facebook API](https://developers.facebook.com/docs/facebook-login "Facebook API") & [Google API](https://developers.google.com/identity/protocols/OAuth2 "Google API"), these are the app IDs you create with Google and Facebook to allow signings to your website.
	- `FACEBOOK_SECRET=` & `GOOGLE_SECRET=` - Secret applicaiton keys that handle authentication with your app. Will be given to you once you register your applications with each platforms API.
	- `FACEBOOK_URL=` & `GOOGLE_URL=` - Call back URLs for your platform. Should be something like [YourDomainHere]/auth/[API Name (ie facebook or google)/callback]
1. Run the [laravel migrations](https://laravel.com/docs/5.6/migrations#running-migrations "laravel migrations") for the application.
1. Enjoy!

#### Support
This app is provided as is with no warranties or support of any kind. Feel free to open any issues, or bring any bugs or feature request to my attention. I will do my best to help where I can but it should be assumed no support will be provided.