function manageSubscription(identifier){
	
	var sub_event = $(identifier).data('event');
	var feed_ID = $(identifier).data('value');
	
	var process_subscription = new XMLHttpRequest();
	var page_protocol = window.location.protocol;
	var domain_hostname = window.location.hostname;
	var url = page_protocol + "//" + domain_hostname + "/";
	console.log("Host URL: " + url);
	
	if(sub_event == 'unsubscribe'){
		url = url + 'remove-subscription/' + feed_ID;
		$(identifier).text('+subscribe');
		$(identifier).removeClass("unsubscribe_btn");
		$(identifier).addClass("subscribe_btn");
		$(identifier).data('event','subscribe');
	}else if(sub_event == 'subscribe'){
		url = url + 'add-subscription/' + feed_ID;
		$(identifier).text('-unsubscribe');
		$(identifier).removeClass("subscribe_btn");
		$(identifier).addClass("unsubscribe_btn");
		$(identifier).data('event','unsubscribe');
	}
	
	process_subscription.open('GET', url);
	process_subscription.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	process_subscription.onload = function() {
		console.log('Response Info: ' + process_subscription.responseText);
	};
	process_subscription.send();
}