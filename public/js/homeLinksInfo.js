$('.homepage-story-block').each(function(){
	var story_url = $(this).find('.homepage-story-link').attr("href");
	
	var domain_data = domain_forms(story_url);
	
	var form_link = '<a href="//' + domain_data.hostname + '" target="_blank" onClick="ga(\'send\',\'event\',\'Home Page\',\'News Source Clicked\',\'' + domain_data.root_domain + '\');">' + domain_data.root_domain + '</a>';
	
	$(this).find('.homepage-story-source').html(form_link);
	$(this).find('.reveal-me').fadeTo(1543,1);
});

/*
	A SERIES OF FUNCTIONS DESIGNED TO GET STORY SOURCE DATA AND POST IT TO THE HOMEPAGE
	Yet again thanks Mr. Stackoverflow: https://stackoverflow.com/questions/8498592/extract-hostname-name-from-string
	Function extracts the domain from each story URL parsed.
*/
function domain_forms(story_url){
	var full_hostname = extractHostname(story_url);
	var extracted_root = extractRootDomain(story_url);
	
	return {hostname:full_hostname,root_domain:extracted_root}
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }else{
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function extractRootDomain(url) {
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain 
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}