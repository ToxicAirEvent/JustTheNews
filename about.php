<!DOCTYPE html>
<html class="" lang="en">

<head>
<title>Just The News RSS Reader</title>
</head>

<body>
    <h1>Why do this?</h1>
    <p>
    One thing I really liked about twitter when it first came to be was how straight forwards it was.
    If you followed a lot of news sites the information came to you in the order it happened. Newest to oldest.
    Twitter and others have diverged from this original idea with algorithms that cater to what they think you want to see.
    </p>
	
	<p>That is all well and good. There are certainly upsides to it.</p>
	
	<p>However we wanted a news reader more like what I originally described twitter as using RSS feeds since most major news outlets have those.
	Sure I could setup an account for myself on something like Feedly but for the sake of learning something I figured I'd take a crack at writing my own
	rss feed parser.</p>
	
	<p>My friend Will is helping me out and we'll see how this goes.</p>
	
	<h4>-Thanks For Reading, Jack.</h4>
	
	<p><a href="./">&lt;&lt;HOME</a></p>
	
	<hr />
	
	<h1>PHP Info To See What Is Enabled</h1>
	
	<?php
	echo $_SERVER["PATH"];
	
	phpinfo();
	
	?>
</body>
</html>